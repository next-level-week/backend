INICIALIZANDO O NODE TYPESCRIPT

Como estamos trabalhando com typescript é necessário ter um compilador para o mesmo, 
além de instalar o observador do código, funcionando como um watcher:

npm i ts-node -D
npm i ts-node-dev -D


Também não esqueça de instalar o typescript e criar seu arquivo de inialização:

npm i typescript-D
npx tsc --init

Após isso, para iniciar esse arquivo será necessário o seguinte comando:

npx ts-node-dev ./src/server.ts