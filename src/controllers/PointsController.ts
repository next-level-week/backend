import { Request, Response } from 'express'
import knex from '../database/connection'

class PointsController {
  async list(request: Request, response: Response) {
    const { city, uf, items } = request.query
  
    const parsedItems = String(items)
      .split(',')
      .map(item => Number(item.trim()))

    const points = await knex('points')
      .join('point_items', 'points.id', '=', 'point_items.point_id')
      .whereIn('point_items.item_id', parsedItems) //se tem algum dos itens
      .where('city', String(city))
      .where('uf', String(uf))
      .distinct()
      .select('points.*')

    const serializedPoints = points.map(point => { 
      return {
        ...point, 
        image_url: point.image.includes('http') ? point.image : '/uploads/' + point.image
      } 
    })

    return response.json(serializedPoints)
  }

  async show(request: Request, response: Response) {
    const { id } = request.params
    
    const point = await knex('points').where('id', id).first()
    // await new Promise((resolve, reject) => 
    //   knex('points').where('id', id).first()
    //   .then(data => resolve(data))
    //   .catch(error => reject(error))
    // )

    if(!point) {
      return response.status(404).json({
        message: `Point with ID:${id} is not found.`
      })
    }

    const serializedPoints = {
      ...point, 
      image_url: point.image.includes('http') ? point.image : '/uploads/' + point.image
    }

    const items = await knex('items')
      .join('point_items', 'items.id', '=', 'point_items.item_id')
      .where('point_items.point_id', id)
      .select('items.title')

    return response.json({ point: serializedPoints, items })
  }

  async create(request: Request, response: Response) {
    const {
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
      items
    } = request.body
  
    const trx = await knex.transaction()
    // substiui o knex, para apenas executar as
    // transações se as anteriores tiverem sucesso.
    // Caso uma delas não tenha, ocorrerá um rollback
  
    const defaultImage = 'https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=200&q=120'

    const point = await
    trx('points').insert({
      image: request.file.filename || defaultImage,
      name,
      email,
      whatsapp,
      latitude,
      longitude,
      city,
      uf,
    })

    const point_id = point[0]
  
    const pointItems = items
    .split(',')
    .map((item: string) => Number(item.trim()))
    .map((item_id: number) => {
      return {
        item_id,
        point_id,
      }
    })
  
    await trx('point_items').insert(pointItems)
  
    await trx.commit()

    return response.json({ id: point_id })
  }

}

export default PointsController