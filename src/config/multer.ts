import multer from 'multer'
import path from 'path'
import crypto from 'crypto'

// Reference: https://github.com/expressjs/multer
// try the 'fileFilter' function

export default {
  storage: multer.diskStorage({
    destination: path.resolve(__dirname, '..', '..', 'uploads'),
    
    filename(request, file, callback) {
      const hash = crypto.randomBytes(6).toString('hex')
      const filename = `${hash}-${file.originalname}`

      callback(null, filename)
    }

  }),
}