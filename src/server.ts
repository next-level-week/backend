import express from 'express'
import cors from 'cors'
import path from 'path'
import routes from './routes'
import { errors } from 'celebrate'

const app = express()

app.use(cors({
  // origin: 'www.site....'
}))
app.use(express.json())
app.use(routes)

app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')))
// exemple: 
// http://localhost:3333/uploads/baterias.svg

app.use(errors())

app.listen(3333)